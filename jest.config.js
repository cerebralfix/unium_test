module.exports = {
	transform: {
		".ts": "<rootDir>/unium_test/node_modules/ts-jest",
	},
	globals: {
		"ts-jest": {
			tsConfig: "tsconfig.json",
		},
	},
	verbose: true,
	moduleFileExtensions: ["js", "ts"],
	testRegex: "./test/.*\\.spec\\.ts$",
	rootDir: "../",
	collectCoverage: true,
	coverageDirectory: "unium_test/game_coverage",
	collectCoverageFrom: ["**/test/*.ts"],
};
