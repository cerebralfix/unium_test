import * as fs from "fs";
import { IPosition, IUniumObject } from "../unium_test/Tests/helper/MessageType";
import { screenshot } from "../unium_test/Tests/helper/Screenshot";
import { wsh } from "../unium_test/Tests/setup/setup-jest";
import { ImageField } from "../unium_test/Tests/types/ImageField";
import { StatsField } from "../unium_test/Tests/types/StatsField";
import * as GameObjects from "./gameObjects";

describe("Main Test", async () => {
	test("Basic product information", async () => {
		const k: IUniumObject = await wsh.about();

		expect(k.Unium).toEqual("1.0");
		expect(k.IsEditor).toBeTruthy();
		expect(k.Product).toEqual("unium");
		expect(k.Company).toEqual("gwaredd");
		expect(k.Version).toEqual("1.0");
		expect(k.Scene).toEqual("Tutorial");
	});

	test.skip("invoke click", async () => {
		await wsh.invokeClick("scene/Canvas/CloseButton");
		await wsh.sleep(1000);

		expect(await wsh.query(`scene/Canvas/CloseButton.LTOCloseButton}.${ImageField.isActiveAndEnabled}`)).toBeFalsy();
	});

	// tslint:disable:max-line-length
	test("Long string vs import from separate class", async () => {
		// Long string
		const isActiveAndEnabled = await wsh.query(
			`scene/GameManagers/UIManager/_Dialogs/Settings/SettingsBG/ScrollView/ScrollPanel/AccountInformation/AccountInfoBtn.${
				ImageField.isActiveAndEnabled
			}`,
		);
		expect(isActiveAndEnabled).toBeFalsy();

		// Import from game object
		expect(await wsh.query(`${GameObjects.AccountInfoBtn}.${ImageField.isActiveAndEnabled}`)).toBeFalsy();
	});

	test.skip("Take a screenshot", async () => {
		try {
			const path: string = __dirname + "/screenshots/test.png";

			screenshot(path);

			expect(fs.existsSync(path)).toBeTruthy();
		} catch (error) {
			console.log(error);
			expect(false).toBeTruthy();
		}
	});

	test.skip("Teleport player", async () => {
		const newPosition: IPosition = { x: 10, y: 0, z: 10 };

		await wsh.teleportObjectToPosition("scene/Game/Player", newPosition);

		const playerPosition: IPosition = await wsh.getGameObjectPosition("scene/Game/Player");

		expect(playerPosition.x).toEqual(newPosition.x);
		expect(playerPosition.z).toEqual(newPosition.z);
	});

	test.skip("FPS above 60 for 4 seconds", async () => {
		const id: string = StatsField.FPS;

		await wsh.repeatQuery(id, StatsField.FPS, 1);

		for (let i = 0; i < 4; i++) {
			const fps = await wsh.waitForResponse(id, undefined);
			if (fps[0] < 60) {
				await wsh.removeQuery(id);
				expect(false).toBeTruthy();
				break;
			}
		}

		await wsh.removeQuery(id);
		expect(true).toBeTruthy();
	});
});
