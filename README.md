This repo make use of [unium](https://github.com/gwaredd/unium) to do end to end test on unity game. The test runner is [jest](https://jestjs.io/docs/en/api)

# Setup

-   Make sure Unity is not running.
-   Have [ npm ](https://www.npmjs.com/) installed

## Install

Run following commands in the unity project folder

    git submodule add git@bitbucket.org:cerebralfix/unium_test.git
    cd unium_test
    npm install
    npm run copyUnium
    npm run copyEditorScript
    npm run copySampleTest

## Unity project

-   Open unity project
-   Create an empty object in main game scene and rename to `Unium`
-   Click `Add Component` in inspector and add `Unium Component (Script)`
    -   Optional: If using the click function using coordinate:
        -   add `Unium Simulate (Script)`
-   Run Game

# Test

## First Test

    cd unium_test
    npm test

-   Expect test to fail
    -   edit `<base folder>/test/mainTest.spec.ts` to have the correct
        -   `Product`
        -   `Company`
        -   `Version`
        -   `Scene`
    -   Test should now pass

## Normal Test Procedure

1. Run the game in editor mode
2. Go to the scene you wanted to test
3. Look at the `Hierachy`
4. In the following example the `AchievementsButton` url will be `"scene/Canvas/BottomButtonPanel/AchievementsButton"`

![hierachy](./images/hierarchy.png)

5. Then open up the link in browser to valid the url and view the object properties (prepend `"http://localhost:8432/q/"`)

![browser](./images/browser.png)

6. After validating the url then it can be used in the test.

> Bonus: Right click on Hierarchy and use `Copy Hierarchy to Clipboard` to save typing it out. It will put a string corresponding to the hierarchy on your clipboard ready to be pasted.

> ![browser](./images/hierarchy_editor.png)

# More Test

## Watch Mode

-   use `npm run test:watch` will allow for test to run on save.
-   use `p` to filter test by filename

## Focus

-   use `test.only` instead of `test` to focus on that test.
-   use `describe.only` or `fdescribe` instead of `describe` to focus on that group of test

## Skip

-   use `test.skip` to skip that test
-   use `describe.skip` or `xdescribe` to skip those tests

## Game Object Explorer

Navigate to `http://localhost:8342/q/scene/` to get all the scene from the game. That is using REST api. Refer to [unium.pdf](https://github.com/gwaredd/unium/raw/master/unium.pdf) for more.

WebSocket API is what unium used, for a tool using WebSocket run

    npm run openSocketExplorer

![socket_explorer](./images/socket_explorer.png)

## (Clone) in name problem

Sometimes you would encounter `Something(Clone)` type of name and unium does not work for that game object. The easiest way is to assign it a name when creating that game object via `Instantiate`.

    UIScreen screen = Instantiate(state.prefab);
    screen.name = "Something";

## Screenshot

-   change `test.skip` to `test` for _`Take a screenshot`_ test in `<base folder>/test/mainTest.spec.ts`
-   npm test
-   A screenshot should be generated in the `screenshots` folder

## Moving Game Object

-   Study _`Move player to pickup object position`_ test
-   Study _`Teleport player`_ test

## Repeated query

This can be useful for waiting for loading screen, waiting for a game object to appear, calculate frame rate etc.

-   Study _`FPS above 60 for 4 seconds`_ test

# Advance

## Prepush Test

Adding the following line into `scripts` in `Automated\package.json`

    "prepush": "npm run test",

## Updating submodule

    git submodule foreach git pull origin master

## Updating unium

There is currently no way to tell if unium is up to date or not. Check the repo for the last commit date and compare to when `package-lock.json` was last modified.

    cd unium_test
    npm upgrade unium
    npm run copyUnium

## Adding more types

Some common types are provide in `unium_test/Tests/types`. When encoutering types not define here please do add to it and the corresponding tests.
