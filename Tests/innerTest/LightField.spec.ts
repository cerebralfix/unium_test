import { LightField } from "../types/LightField";

test("LightField", async () => {
	expect(LightField.value).toBe("Light");
	expect(LightField.shadowStrength).toBe("Light.shadowStrength");
	expect(LightField.cookieSize).toBe("Light.cookieSize");
	expect(LightField.commandBufferCount).toBe("Light.commandBufferCount");
	expect(LightField.spotAngle).toBe("Light.spotAngle");
	expect(LightField.colorTemperature).toBe("Light.colorTemperature");
	expect(LightField.intensity).toBe("Light.intensity");
	expect(LightField.bounceIntensity).toBe("Light.bounceIntensity");
	expect(LightField.shadowCustomResolution).toBe("Light.shadowCustomResolution");
	expect(LightField.shadowBias).toBe("Light.shadowBias");
	expect(LightField.shadowNormalBias).toBe("Light.shadowNormalBias");
	expect(LightField.shadowNearPlane).toBe("Light.shadowNearPlane");
	expect(LightField.range).toBe("Light.range");
	expect(LightField.cullingMask).toBe("Light.cullingMask");
	expect(LightField.shadowRadius).toBe("Light.shadowRadius");
	expect(LightField.shadowAngle).toBe("Light.shadowAngle");
	expect(LightField.enabled).toBe("Light.enabled");
	expect(LightField.isActiveAndEnabled).toBe("Light.isActiveAndEnabled");
	expect(LightField.tag).toBe("Light.tag");
	expect(LightField.Name).toBe("Light.name");
});
