import { TransformField } from "../types/TransformField";

test("TransformField", async () => {
	expect(TransformField.value).toBe("Transform");
	expect(TransformField.Name).toBe("Transform.name");

	expect(TransformField.position.value).toBe("Transform.position");
	expect(TransformField.position.x).toBe("Transform.position.x");
	expect(TransformField.position.y).toBe("Transform.position.y");
	expect(TransformField.position.z).toBe("Transform.position.z");

	expect(TransformField.rotation.value).toBe("Transform.rotation");
	expect(TransformField.rotation.x).toBe("Transform.rotation.x");
	expect(TransformField.rotation.y).toBe("Transform.rotation.y");
	expect(TransformField.rotation.z).toBe("Transform.rotation.z");
	expect(TransformField.rotation.w).toBe("Transform.rotation.w");

	expect(TransformField.scale.value).toBe("Transform.scale");
	expect(TransformField.scale.x).toBe("Transform.scale.x");
	expect(TransformField.scale.y).toBe("Transform.scale.y");
	expect(TransformField.scale.z).toBe("Transform.scale.z");
});
