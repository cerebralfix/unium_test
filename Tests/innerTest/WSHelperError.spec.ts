import { Server } from "mock-socket";
import WebSocketHelper from "../helper/WSHelper";

const fakeURL = "ws://localhost:8342/ws";
let mockServer;

test("Failed to connect", async () => {
	const wsh = new WebSocketHelper();
	expect(wsh.connect()).rejects.toEqual("Failed to Connect");
});

xdescribe("WSHelper", async () => {
	beforeEach(() => {
		if (mockServer) {
			mockServer.close();
		}
		mockServer = new Server(fakeURL);
	});

	test("duplicate query id", async () => {
		const id: string = "test";
		const repeating: string = "repeating";
		mockServer.on("connection", (socket) => {
			socket.on("message", () => {
				socket.send(JSON.stringify({ id, info: repeating, data: {} }));
			});
		});

		try {
			const wsh = new WebSocketHelper();
			await wsh.connect();
			await wsh.repeatQuery(id, "/about", 1);
			await wsh.repeatQuery(id, "/about", 1);
		} catch (error) {
			expect(error).toEqual({
				error: "Repeating query ID (test) is already being used",
			});
		}
	});

	test("simulate Click Drag Error", async () => {
		const wsh = new WebSocketHelper();
		await wsh.connect();

		// Click < 0
		expect(wsh.simulateClick(-1, 10)).rejects.toEqual({ error: "invalid input range" });
		expect(wsh.simulateClick(10, -1)).rejects.toEqual({ error: "invalid input range" });

		// Click Normalised < 0
		expect(wsh.simulateClickNormalised(-1, 1)).rejects.toEqual({
			error: "invalid input range",
		});
		expect(wsh.simulateClickNormalised(1, -1)).rejects.toEqual({
			error: "invalid input range",
		});

		// Click Normalised > 1
		expect(wsh.simulateClickNormalised(2, 1)).rejects.toEqual({
			error: "invalid input range",
		});
		expect(wsh.simulateClickNormalised(1, 2)).rejects.toEqual({
			error: "invalid input range",
		});

		// Drag < 0
		expect(wsh.simulateDrag(-1, 10, 10, 10, 1)).rejects.toEqual({
			error: "invalid input range",
		});
		expect(wsh.simulateDrag(10, -1, 10, 10, 1)).rejects.toEqual({
			error: "invalid input range",
		});
		expect(wsh.simulateDrag(10, 10, -1, 10, 1)).rejects.toEqual({
			error: "invalid input range",
		});
		expect(wsh.simulateDrag(10, 10, 10, -1, 1)).rejects.toEqual({
			error: "invalid input range",
		});
		expect(wsh.simulateDrag(10, 10, 10, 10, -1)).rejects.toEqual({
			error: "invalid input range",
		});

		// Drag Normalised < 0
		expect(wsh.simulateDragNormalised(-1, 1, 1, 1, 1)).rejects.toEqual({
			error: "invalid input range",
		});
		expect(wsh.simulateDragNormalised(1, -1, 1, 1, 1)).rejects.toEqual({
			error: "invalid input range",
		});
		expect(wsh.simulateDragNormalised(1, 1, -1, 1, 1)).rejects.toEqual({
			error: "invalid input range",
		});
		expect(wsh.simulateDragNormalised(1, 1, 1, -1, 1)).rejects.toEqual({
			error: "invalid input range",
		});
		expect(wsh.simulateDragNormalised(1, 1, 1, 1, -1)).rejects.toEqual({
			error: "invalid input range",
		});

		// Drag Normalised > 1
		expect(wsh.simulateDragNormalised(2, 1, 1, 1, 1)).rejects.toEqual({
			error: "invalid input range",
		});
		expect(wsh.simulateDragNormalised(1, 2, 1, 1, 1)).rejects.toEqual({
			error: "invalid input range",
		});
		expect(wsh.simulateDragNormalised(1, 1, 2, 1, 1)).rejects.toEqual({
			error: "invalid input range",
		});
		expect(wsh.simulateDragNormalised(1, 1, 1, 2, 1)).rejects.toEqual({
			error: "invalid input range",
		});
	});
});
