import { MeshRendererField } from "../types/MeshRendererField";

test("MeshRendererField", async () => {
	expect(MeshRendererField.value).toBe("MeshRenderer");
	expect(MeshRendererField.subMeshStartIndex).toBe("MeshRenderer.subMeshStartIndex");
	expect(MeshRendererField.enabled).toBe("MeshRenderer.enabled");
	expect(MeshRendererField.isVisible).toBe("MeshRenderer.isVisible");
	expect(MeshRendererField.receiveShadows).toBe("MeshRenderer.receiveShadows");
	expect(MeshRendererField.renderingLayerMask).toBe("MeshRenderer.renderingLayerMask");
	expect(MeshRendererField.sortingLayerName).toBe("MeshRenderer.sortingLayerName");
	expect(MeshRendererField.sortingLayerID).toBe("MeshRenderer.sortingLayerID");
	expect(MeshRendererField.sortingOrder).toBe("MeshRenderer.sortingOrder");
	expect(MeshRendererField.allowOcclusionWhenDynamic).toBe(
		"MeshRenderer.allowOcclusionWhenDynamic",
	);
	expect(MeshRendererField.isPartOfStaticBatch).toBe("MeshRenderer.isPartOfStaticBatch");
	expect(MeshRendererField.lightmapIndex).toBe("MeshRenderer.lightmapIndex");
	expect(MeshRendererField.realtimeLightmapIndex).toBe("MeshRenderer.realtimeLightmapIndex");
	expect(MeshRendererField.tag).toBe("MeshRenderer.tag");
	expect(MeshRendererField.Name).toBe("MeshRenderer.name");
});
