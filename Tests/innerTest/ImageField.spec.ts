import { ImageField } from "../types/ImageField";

test("ImageField", async () => {
	expect(ImageField.value).toBe("Image");
	expect(ImageField.preserveAspect).toBe("Image.preserveAspect");
	expect(ImageField.fillCenter).toBe("Image.fillCenter");
	expect(ImageField.fillAmount).toBe("Image.fillAmount");
	expect(ImageField.fillClockwise).toBe("Image.fillClockwise");
	expect(ImageField.fillOrigin).toBe("Image.fillOrigin");
	expect(ImageField.alphaHitTestMinimumThreshold).toBe("Image.alphaHitTestMinimumThreshold");
	expect(ImageField.hasBorder).toBe("Image.hasBorder");
	expect(ImageField.pixelsPerUnit).toBe("Image.pixelsPerUnit");
	expect(ImageField.minWidth).toBe("Image.minWidth");
	expect(ImageField.preferredWidth).toBe("Image.preferredWidth");
	expect(ImageField.flexibleWidth).toBe("Image.flexibleWidth");
	expect(ImageField.minHeight).toBe("Image.minHeight");
	expect(ImageField.preferredHeight).toBe("Image.preferredHeight");
	expect(ImageField.flexibleHeight).toBe("Image.flexibleHeight");
	expect(ImageField.layoutPriority).toBe("Image.layoutPriority");
	expect(ImageField.maskable).toBe("Image.maskable");
	expect(ImageField.raycastTarget).toBe("Image.raycastTarget");
	expect(ImageField.depth).toBe("Image.depth");
	expect(ImageField.useGUILayout).toBe("Image.useGUILayout");
	expect(ImageField.runInEditMode).toBe("Image.runInEditMode");
	expect(ImageField.enabled).toBe("Image.enabled");
	expect(ImageField.isActiveAndEnabled).toBe("Image.isActiveAndEnabled");
	expect(ImageField.tag).toBe("Image.tag");
	expect(ImageField.Name).toBe("Image.name");
});
