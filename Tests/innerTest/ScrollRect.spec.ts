import { ScrollRect } from "../types/ScrollRect";

test("ScrollRect", async () => {
	expect(ScrollRect.value).toBe("ScrollRect");
	expect(ScrollRect.horizontal).toBe("ScrollRect.horizontal");
	expect(ScrollRect.vertical).toBe("ScrollRect.vertical");
	expect(ScrollRect.elasticity).toBe("ScrollRect.elasticity");
	expect(ScrollRect.inertia).toBe("ScrollRect.inertia");
	expect(ScrollRect.decelerationRate).toBe("ScrollRect.decelerationRate");
	expect(ScrollRect.scrollSensitivity).toBe("ScrollRect.scrollSensitivity");
	expect(ScrollRect.horizontalScrollbarSpacing).toBe("ScrollRect.horizontalScrollbarSpacing");
	expect(ScrollRect.verticalScrollbarSpacing).toBe("ScrollRect.verticalScrollbarSpacing");
	expect(ScrollRect.horizontalNormalizedPosition).toBe("ScrollRect.horizontalNormalizedPosition");
	expect(ScrollRect.verticalNormalizedPosition).toBe("ScrollRect.verticalNormalizedPosition");

	expect(ScrollRect.minWidth).toBe("ScrollRect.minWidth");
	expect(ScrollRect.preferredWidth).toBe("ScrollRect.preferredWidth");
	expect(ScrollRect.flexibleWidth).toBe("ScrollRect.flexibleWidth");
	expect(ScrollRect.minHeight).toBe("ScrollRect.minHeight");
	expect(ScrollRect.preferredHeight).toBe("ScrollRect.preferredHeight");
	expect(ScrollRect.flexibleHeight).toBe("ScrollRect.flexibleHeight");
	expect(ScrollRect.layoutPriority).toBe("ScrollRect.layoutPriority");
	expect(ScrollRect.useGUILayout).toBe("ScrollRect.useGUILayout");
	expect(ScrollRect.runInEditMode).toBe("ScrollRect.runInEditMode");
	expect(ScrollRect.enabled).toBe("ScrollRect.enabled");
	expect(ScrollRect.isActiveAndEnabled).toBe("ScrollRect.isActiveAndEnabled");
	expect(ScrollRect.tag).toBe("ScrollRect.tag");
	expect(ScrollRect.Name).toBe("ScrollRect.name");
});
