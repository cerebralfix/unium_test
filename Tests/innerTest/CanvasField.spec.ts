import { CanvasField } from "../types/CanvasField";

test("CanvasField", async () => {
	expect(CanvasField.value).toBe("Canvas");
	expect(CanvasField.isRootCanvas).toBe("Canvas.isRootCanvas");
	expect(CanvasField.scaleFactor).toBe("Canvas.scaleFactor");
	expect(CanvasField.referencePixelsPerUnit).toBe("Canvas.referencePixelsPerUnit");
	expect(CanvasField.overridePixelPerfect).toBe("Canvas.overridePixelPerfect");
	expect(CanvasField.pixelPerfect).toBe("Canvas.pixelPerfect");
	expect(CanvasField.planeDistance).toBe("Canvas.planeDistance");
	expect(CanvasField.renderOrder).toBe("Canvas.renderOrder");
	expect(CanvasField.overrideSorting).toBe("Canvas.overrideSorting");
	expect(CanvasField.sortingOrder).toBe("Canvas.sortingOrder");
	expect(CanvasField.targetDisplay).toBe("Canvas.targetDisplay");
	expect(CanvasField.sortingLayerID).toBe("Canvas.sortingLayerID");
	expect(CanvasField.cachedSortingLayerValue).toBe("Canvas.cachedSortingLayerValue");
	expect(CanvasField.sortingLayerName).toBe("Canvas.sortingLayerName");
	expect(CanvasField.normalizedSortingGridSize).toBe("Canvas.normalizedSortingGridSize");
	expect(CanvasField.enabled).toBe("Canvas.enabled");
	expect(CanvasField.isActiveAndEnabled).toBe("Canvas.isActiveAndEnabled");
	expect(CanvasField.tag).toBe("Canvas.tag");
	expect(CanvasField.Name).toBe("Canvas.name");
});
