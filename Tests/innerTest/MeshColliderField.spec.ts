import { MeshColliderField } from "../types/MeshColliderField";

test("MeshColliderField", async () => {
	expect(MeshColliderField.value).toBe("MeshCollider");
	expect(MeshColliderField.convex).toBe("MeshCollider.convex");
	expect(MeshColliderField.inflateMesh).toBe("MeshCollider.inflateMesh");
	expect(MeshColliderField.skinWidth).toBe("MeshCollider.skinWidth");
	expect(MeshColliderField.enabled).toBe("MeshCollider.enabled");
	expect(MeshColliderField.isTrigger).toBe("MeshCollider.isTrigger");
	expect(MeshColliderField.contactOffset).toBe("MeshCollider.contactOffset");
	expect(MeshColliderField.tag).toBe("MeshCollider.tag");
	expect(MeshColliderField.Name).toBe("MeshCollider.name");
});
