import { ParticleSystemField } from "../types/ParticleSystemField";

test("ParticleSystemField", async () => {
	expect(ParticleSystemField.value).toBe("ParticleSystem");
	expect(ParticleSystemField.isPlaying).toBe("ParticleSystem.isPlaying");
	expect(ParticleSystemField.isEmitting).toBe("ParticleSystem.isEmitting");
	expect(ParticleSystemField.isStopped).toBe("ParticleSystem.isStopped");
	expect(ParticleSystemField.time).toBe("ParticleSystem.time");
	expect(ParticleSystemField.particleCount).toBe("ParticleSystem.particleCount");
	expect(ParticleSystemField.randomSeed).toBe("ParticleSystem.randomSeed");
	expect(ParticleSystemField.useAutoRandomSeed).toBe("ParticleSystem.useAutoRandomSeed");
	expect(ParticleSystemField.automaticCullingEnabled).toBe(
		"ParticleSystem.automaticCullingEnabled",
	);
	expect(ParticleSystemField.tag).toBe("ParticleSystem.tag");
	expect(ParticleSystemField.Name).toBe("ParticleSystem.name");
});
