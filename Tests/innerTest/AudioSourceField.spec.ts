import { AudioSourceField } from "../types/AudioSourceField";

test("AudioSourceField", async () => {
	expect(AudioSourceField.value).toBe("AudioSource");
	expect(AudioSourceField.volume).toBe("AudioSource.volume");
	expect(AudioSourceField.pitch).toBe("AudioSource.pitch");
	expect(AudioSourceField.time).toBe("AudioSource.time");
	expect(AudioSourceField.timeSamples).toBe("AudioSource.timeSamples");
	expect(AudioSourceField.isPlaying).toBe("AudioSource.isPlaying");
	expect(AudioSourceField.isVirtual).toBe("AudioSource.isVirtual");
	expect(AudioSourceField.loop).toBe("AudioSource.loop");
	expect(AudioSourceField.ignoreListenerVolume).toBe("AudioSource.ignoreListenerVolume");
	expect(AudioSourceField.playOnAwake).toBe("AudioSource.playOnAwake");
	expect(AudioSourceField.ignoreListenerPause).toBe("AudioSource.ignoreListenerPause");
	expect(AudioSourceField.panStereo).toBe("AudioSource.panStereo");
	expect(AudioSourceField.spatialBlend).toBe("AudioSource.spatialBlend");
	expect(AudioSourceField.spatialize).toBe("AudioSource.spatialize");
	expect(AudioSourceField.spatializePostEffects).toBe("AudioSource.spatializePostEffects");
	expect(AudioSourceField.reverbZoneMix).toBe("AudioSource.reverbZoneMix");
	expect(AudioSourceField.bypassEffects).toBe("AudioSource.bypassEffects");
	expect(AudioSourceField.bypassListenerEffects).toBe("AudioSource.bypassListenerEffects");
	expect(AudioSourceField.bypassReverbZones).toBe("AudioSource.bypassReverbZones");
	expect(AudioSourceField.dopplerLevel).toBe("AudioSource.dopplerLevel");
	expect(AudioSourceField.spread).toBe("AudioSource.spread");
	expect(AudioSourceField.priority).toBe("AudioSource.priority");
	expect(AudioSourceField.mute).toBe("AudioSource.mute");
	expect(AudioSourceField.minDistance).toBe("AudioSource.minDistance");
	expect(AudioSourceField.maxDistance).toBe("AudioSource.maxDistance");

	expect(AudioSourceField.enabled).toBe("AudioSource.enabled");
	expect(AudioSourceField.isActiveAndEnabled).toBe("AudioSource.isActiveAndEnabled");
	expect(AudioSourceField.tag).toBe("AudioSource.tag");
	expect(AudioSourceField.Name).toBe("AudioSource.name");
});
