import { CameraField } from "../types/CameraField";

test("CameraField", async () => {
	expect(CameraField.value).toBe("Camera");
	expect(CameraField.nearClipPlane).toBe("Camera.nearClipPlane");
	expect(CameraField.farClipPlane).toBe("Camera.farClipPlane");
	expect(CameraField.allowHDR).toBe("Camera.allowHDR");
	expect(CameraField.allowDynamicResolution).toBe("Camera.allowDynamicResolution");
	expect(CameraField.forceIntoRenderTexture).toBe("Camera.forceIntoRenderTexture");
	expect(CameraField.orthographicSize).toBe("Camera.orthographicSize");
	expect(CameraField.orthographic).toBe("Camera.orthographic");
	expect(CameraField.depth).toBe("Camera.depth");
	expect(CameraField.aspect).toBe("Camera.aspect");
	expect(CameraField.cullingMask).toBe("Camera.cullingMask");
	expect(CameraField.eventMask).toBe("Camera.eventMask");
	expect(CameraField.layerCullSpherical).toBe("Camera.layerCullSpherical");
	expect(CameraField.useOcclusionCulling).toBe("Camera.useOcclusionCulling");
	expect(CameraField.clearStencilAfterLightingPass).toBe("Camera.clearStencilAfterLightingPass");
	expect(CameraField.usePhysicalProperties).toBe("Camera.usePhysicalProperties");
	expect(CameraField.focalLength).toBe("Camera.focalLength");
	expect(CameraField.pixelWidth).toBe("Camera.pixelWidth");
	expect(CameraField.pixelHeight).toBe("Camera.pixelHeight");
	expect(CameraField.scaledPixelWidth).toBe("Camera.scaledPixelWidth");
	expect(CameraField.scaledPixelHeight).toBe("Camera.scaledPixelHeight");
	expect(CameraField.targetDisplay).toBe("Camera.targetDisplay");
	expect(CameraField.useJitteredProjectionMatrixForTransparentRendering).toBe(
		"Camera.useJitteredProjectionMatrixForTransparentRendering",
	);
	expect(CameraField.stereoEnabled).toBe("Camera.stereoEnabled");
	expect(CameraField.stereoSeparation).toBe("Camera.stereoSeparation");
	expect(CameraField.stereoConvergence).toBe("Camera.stereoConvergence");
	expect(CameraField.areVRStereoViewMatricesWithinSingleCullTolerance).toBe(
		"Camera.areVRStereoViewMatricesWithinSingleCullTolerance",
	);
	expect(CameraField.allCamerasCount).toBe("Camera.allCamerasCount");
	expect(CameraField.commandBufferCount).toBe("Camera.commandBufferCount");

	expect(CameraField.enabled).toBe("Camera.enabled");
	expect(CameraField.isActiveAndEnabled).toBe("Camera.isActiveAndEnabled");
	expect(CameraField.tag).toBe("Camera.tag");
	expect(CameraField.Name).toBe("Camera.name");
});
