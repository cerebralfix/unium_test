import { CanvasRendererField } from "../types/CanvasRendererField";

test("CanvasRendererField", async () => {
	expect(CanvasRendererField.value).toBe("CanvasRenderer");
	expect(CanvasRendererField.hasPopInstruction).toBe("CanvasRenderer.hasPopInstruction");
	expect(CanvasRendererField.materialCount).toBe("CanvasRenderer.materialCount");
	expect(CanvasRendererField.popMaterialCount).toBe("CanvasRenderer.popMaterialCount");
	expect(CanvasRendererField.absoluteDepth).toBe("CanvasRenderer.absoluteDepth");
	expect(CanvasRendererField.hasMoved).toBe("CanvasRenderer.hasMoved");
	expect(CanvasRendererField.cullTransparentMesh).toBe("CanvasRenderer.cullTransparentMesh");
	expect(CanvasRendererField.hasRectClipping).toBe("CanvasRenderer.hasRectClipping");
	expect(CanvasRendererField.relativeDepth).toBe("CanvasRenderer.relativeDepth");
	expect(CanvasRendererField.cull).toBe("CanvasRenderer.cull");
	expect(CanvasRendererField.tag).toBe("CanvasRenderer.tag");
	expect(CanvasRendererField.Name).toBe("CanvasRenderer.name");
});
