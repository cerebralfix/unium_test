import { Server } from "mock-socket";
import { IPosition, IUniumObject } from "../helper/MessageType";
import WebSocketHelper from "../helper/WSHelper";

const fakeURL = "ws://localhost:8342/ws";
const gameObject: string = "abc";
let mockServer;

xdescribe("WSHelper", async () => {
	beforeEach(() => {
		if (mockServer) {
			mockServer.close();
		}
		mockServer = new Server(fakeURL);
	});

	test("about", async () => {
		const about: IUniumObject = {
			Unium: "1.0",
			IsEditor: false,
			Product: "Product",
			Company: "Company",
			Version: "1.0",
			Scene: "Tutorial",
		};

		mockServer.on("connection", (socket) => {
			socket.on("message", (data) => {
				const newData = JSON.parse(data);
				expect(newData.id).toBe("about");
				expect(newData.q).toBe("/about");
				socket.send(JSON.stringify({ id: newData.id, data: about }));
			});
		});

		const wsh = new WebSocketHelper();
		await wsh.connect();
		const returnMsg = await wsh.about();

		expect(returnMsg).toEqual(about);
	});

	test("getGameObjectPosition", async () => {
		const position: IPosition = {
			x: Math.random(),
			y: Math.random(),
			z: Math.random(),
		};
		mockServer.on("connection", (socket) => {
			socket.on("message", (data) => {
				const newData = JSON.parse(data);
				expect(newData.id).toEqual(expect.stringContaining(`${gameObject}_position`));
				expect(newData.q).toEqual(`/q/${gameObject}.Transform.position`);
				socket.send(JSON.stringify({ id: newData.id, data: [position] }));
			});
		});

		const wsh = new WebSocketHelper();
		await wsh.connect();
		const returnMsg = await wsh.getGameObjectPosition(gameObject);

		expect(returnMsg).toEqual(position);
	});

	test("teleportObjectToPosition", async () => {
		const position: IPosition = {
			x: Math.random(),
			y: Math.random(),
			z: Math.random(),
		};
		const teleportTo: string =
			"Transform.position={'x':" +
			position.x +
			", 'y':" +
			position.y +
			", 'z':" +
			position.z +
			"}";

		mockServer.on("connection", (socket) => {
			socket.on("message", (data) => {
				const newData = JSON.parse(data);
				expect(newData.id).toEqual(expect.stringContaining(`teleport_${gameObject}_`));
				expect(newData.q).toBe(`/q/${gameObject}.${teleportTo}`);
				socket.send(JSON.stringify({ id: newData.id, data: [] }));
			});
		});

		const wsh = new WebSocketHelper();
		await wsh.connect();
		const returnMsg = await wsh.teleportObjectToPosition(gameObject, position);

		expect(returnMsg).toBeUndefined();
	});

	test("query", async () => {
		mockServer.on("connection", (socket) => {
			socket.on("message", (data) => {
				const newData = JSON.parse(data);
				expect(newData.id).toEqual(expect.stringContaining(`query_${gameObject}_`));
				expect(newData.q).toBe(`/q/${gameObject}`);
				socket.send(JSON.stringify({ id: newData.id, data: {} }));
			});
		});

		const wsh = new WebSocketHelper();
		await wsh.connect();
		const returnMsg = await wsh.query(gameObject);

		expect(returnMsg).toBeUndefined();
	});

	test("repeatQuery", async () => {
		const id: string = "test";
		const repeating: string = "repeating";

		mockServer.on("connection", (socket) => {
			socket.on("message", (data) => {
				const newData = JSON.parse(data);
				expect(newData.id).toBe(id);
				expect(newData.q).toBe(`/q/${gameObject}`);
				expect(newData.repeat.freq).toBe(1);
				socket.send(JSON.stringify({ id, info: repeating, data: {} }));
			});
		});

		const wsh = new WebSocketHelper();
		await wsh.connect();
		const returnMsg = await wsh.repeatQuery(id, gameObject);

		expect(returnMsg).toEqual(repeating);
	});
});
