import { StatsField } from "../types/StatsField";

test("StatsField", async () => {
	expect(StatsField.value).toBe("stats");
	expect(StatsField.FPS).toBe("stats.FPS");
	expect(StatsField.RunningTime).toBe("stats.RunningTime");
	expect(StatsField.LevelTime).toBe("stats.LevelTime");
	expect(StatsField.Scene).toBe("stats.Scene");
});
