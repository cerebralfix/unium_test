import { RectTransformField } from "../types/RectTransformField";

test("RectTransformField", async () => {
	expect(RectTransformField.value).toBe("RectTransform");
	expect(RectTransformField.Name).toBe("RectTransform.name");

	expect(RectTransformField.position.value).toBe("RectTransform.position");
	expect(RectTransformField.position.x).toBe("RectTransform.position.x");
	expect(RectTransformField.position.y).toBe("RectTransform.position.y");
	expect(RectTransformField.position.z).toBe("RectTransform.position.z");

	expect(RectTransformField.rotation.value).toBe("RectTransform.rotation");
	expect(RectTransformField.rotation.x).toBe("RectTransform.rotation.x");
	expect(RectTransformField.rotation.y).toBe("RectTransform.rotation.y");
	expect(RectTransformField.rotation.z).toBe("RectTransform.rotation.z");
	expect(RectTransformField.rotation.w).toBe("RectTransform.rotation.w");

	expect(RectTransformField.scale.value).toBe("RectTransform.scale");
	expect(RectTransformField.scale.x).toBe("RectTransform.scale.x");
	expect(RectTransformField.scale.y).toBe("RectTransform.scale.y");
	expect(RectTransformField.scale.z).toBe("RectTransform.scale.z");
});
