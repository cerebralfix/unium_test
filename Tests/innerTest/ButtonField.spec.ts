import { ButtonField } from "../types/ButtonField";

test("ButtonField", async () => {
	expect(ButtonField.value).toBe("Button");
	expect(ButtonField.interactable).toBe("Button.interactable");
	expect(ButtonField.useGUILayout).toBe("Button.useGUILayout");
	expect(ButtonField.runInEditMode).toBe("Button.runInEditMode");

	expect(ButtonField.enabled).toBe("Button.enabled");
	expect(ButtonField.isActiveAndEnabled).toBe("Button.isActiveAndEnabled");
	expect(ButtonField.tag).toBe("Button.tag");
	expect(ButtonField.Name).toBe("Button.name");
});
