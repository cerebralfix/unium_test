import { AudioListenerField } from "../types/AudioListenerField";

test("AudioListenerField", async () => {
	expect(AudioListenerField.value).toBe("AudioListener");
	expect(AudioListenerField.volume).toBe("AudioListener.volume");
	expect(AudioListenerField.pause).toBe("AudioListener.pause");

	expect(AudioListenerField.enabled).toBe("AudioListener.enabled");
	expect(AudioListenerField.isActiveAndEnabled).toBe("AudioListener.isActiveAndEnabled");
	expect(AudioListenerField.tag).toBe("AudioListener.tag");
	expect(AudioListenerField.Name).toBe("AudioListener.name");
});
