import { Server } from "mock-socket";
import WebSocketHelper from "../helper/WSHelper";

const fakeURL = "ws://localhost:8342/ws";
const gameObject: string = "abc";
let mockServer;

xdescribe("WSHelper", async () => {
	beforeEach(() => {
		if (mockServer) {
			mockServer.close();
		}
		mockServer = new Server(fakeURL);
	});

	test("invokeClick", async () => {
		mockServer.on("connection", (socket) => {
			socket.on("message", (data) => {
				const newData = JSON.parse(data);
				expect(newData.id).toEqual(expect.stringContaining(`invoke_click_${gameObject}_`));
				expect(newData.q).toBe(`/q/${gameObject}.Button.onClick.Invoke()`);
				socket.send(JSON.stringify({ id: newData.id, data: [] }));
			});
		});

		const wsh = new WebSocketHelper();
		await wsh.connect();
		const returnMsg = await wsh.invokeClick(gameObject);

		expect(returnMsg).toBeUndefined();
	});

	test("simulateClick", async () => {
		const point = { x: 100, y: 200 };
		mockServer.on("connection", (socket) => {
			socket.on("message", (data) => {
				const newData = JSON.parse(data);
				expect(newData.id).toEqual(expect.stringContaining(`click_${point.x}_${point.y}_`));
				expect(newData.q).toBe(`/q/simulate.click(${point.x}, ${point.y})`);
				socket.send(JSON.stringify({ id: newData.id, data: [] }));
			});
		});

		const wsh = new WebSocketHelper();
		await wsh.connect();
		const returnMsg = await wsh.simulateClick(point.x, point.y);

		expect(returnMsg).toBeUndefined();
	});

	test("simulateClickNormalised", async () => {
		const point = { x: 0.1, y: 0.1 };
		mockServer.on("connection", (socket) => {
			socket.on("message", (data) => {
				const newData = JSON.parse(data);
				expect(newData.id).toEqual(expect.stringContaining(`click_${point.x}_${point.y}_`));
				expect(newData.q).toBe(`/q/simulate.clickNormalised(${point.x}, ${point.y})`);
				socket.send(JSON.stringify({ id: newData.id, data: [] }));
			});
		});

		const wsh = new WebSocketHelper();
		await wsh.connect();
		const returnMsg = await wsh.simulateClickNormalised(point.x, point.y);

		expect(returnMsg).toBeUndefined();
	});

	test("simulateDrag", async () => {
		const from = { x: 100, y: 100 };
		const to = { x: 200, y: 200 };
		const duration = 2.2;

		mockServer.on("connection", (socket) => {
			socket.on("message", (data) => {
				const newData = JSON.parse(data);
				expect(newData.id).toEqual(
					expect.stringContaining(
						`drag_${from.x}_${from.y}_${to.x}_${to.y}_${duration}_`,
					),
				);
				expect(newData.q).toBe(
					`/q/simulate.drag(${from.x}, ${from.y}, ${to.x}, ${to.y}, ${duration})`,
				);
				socket.send(JSON.stringify({ id: newData.id, data: [] }));
			});
		});

		const wsh = new WebSocketHelper();
		await wsh.connect();
		const returnMsg = await wsh.simulateDrag(from.x, from.y, to.x, to.y, duration);

		expect(returnMsg).toBeUndefined();
	});

	test("simulateDrag", async () => {
		const from = { x: 0.1, y: 0.1 };
		const to = { x: 0.2, y: 0.2 };
		const duration = 2.2;

		mockServer.on("connection", (socket) => {
			socket.on("message", (data) => {
				const newData = JSON.parse(data);
				expect(newData.id).toEqual(
					expect.stringContaining(
						`drag_${from.x}_${from.y}_${to.x}_${to.y}_${duration}_`,
					),
				);
				expect(newData.q).toBe(
					`/q/simulate.dragNormalised(${from.x}, ${from.y}, ${to.x}, ${
						to.y
					}, ${duration})`,
				);
				socket.send(JSON.stringify({ id: newData.id, data: [] }));
			});
		});

		const wsh = new WebSocketHelper();
		await wsh.connect();
		const returnMsg = await wsh.simulateDragNormalised(from.x, from.y, to.x, to.y, duration);

		expect(returnMsg).toBeUndefined();
	});
});
