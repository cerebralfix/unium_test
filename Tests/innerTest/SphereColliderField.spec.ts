import { SphereColliderField } from "../types/SphereColliderField";

test("SphereColliderField", async () => {
	expect(SphereColliderField.value).toBe("SphereCollider");
	expect(SphereColliderField.radius).toBe("SphereCollider.radius");
	expect(SphereColliderField.enabled).toBe("SphereCollider.enabled");
	expect(SphereColliderField.isTrigger).toBe("SphereCollider.isTrigger");
	expect(SphereColliderField.contactOffset).toBe("SphereCollider.contactOffset");
	expect(SphereColliderField.tag).toBe("SphereCollider.tag");
	expect(SphereColliderField.Name).toBe("SphereCollider.name");
});
