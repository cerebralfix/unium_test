import { RigidbodyField } from "../types/RigidbodyField";

test("RigidbodyField", async () => {
	expect(RigidbodyField.value).toBe("Rigidbody");
	expect(RigidbodyField.drag).toBe("Rigidbody.drag");
	expect(RigidbodyField.angularDrag).toBe("Rigidbody.angularDrag");
	expect(RigidbodyField.mass).toBe("Rigidbody.mass");
	expect(RigidbodyField.useGravity).toBe("Rigidbody.useGravity");
	expect(RigidbodyField.maxDepenetrationVelocity).toBe("Rigidbody.maxDepenetrationVelocity");
	expect(RigidbodyField.isKinematic).toBe("Rigidbody.isKinematic");
	expect(RigidbodyField.freezeRotation).toBe("Rigidbody.freezeRotation");
	expect(RigidbodyField.detectCollisions).toBe("Rigidbody.detectCollisions");
	expect(RigidbodyField.solverIterations).toBe("Rigidbody.solverIterations");
	expect(RigidbodyField.sleepThreshold).toBe("Rigidbody.sleepThreshold");
	expect(RigidbodyField.maxAngularVelocity).toBe("Rigidbody.maxAngularVelocity");
	expect(RigidbodyField.solverVelocityIterations).toBe("Rigidbody.solverVelocityIterations");
	expect(RigidbodyField.tag).toBe("Rigidbody.tag");
	expect(RigidbodyField.Name).toBe("Rigidbody.name");
});
