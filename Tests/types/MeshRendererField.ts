import { dot, enabled, isVisible, nameField, tag } from "./commonFields";

const meshRenderer = "MeshRenderer";

export class MeshRendererField {
	public static readonly value = meshRenderer;

	public static readonly subMeshStartIndex = [meshRenderer, "subMeshStartIndex"].join(dot);
	public static readonly enabled = [meshRenderer, enabled].join(dot);
	public static readonly isVisible = [meshRenderer, isVisible].join(dot);
	public static readonly receiveShadows = [meshRenderer, "receiveShadows"].join(dot);
	public static readonly renderingLayerMask = [meshRenderer, "renderingLayerMask"].join(dot);
	public static readonly sortingLayerName = [meshRenderer, "sortingLayerName"].join(dot);
	public static readonly sortingLayerID = [meshRenderer, "sortingLayerID"].join(dot);
	public static readonly sortingOrder = [meshRenderer, "sortingOrder"].join(dot);
	public static readonly allowOcclusionWhenDynamic = [
		meshRenderer,
		"allowOcclusionWhenDynamic",
	].join(dot);
	public static readonly isPartOfStaticBatch = [meshRenderer, "isPartOfStaticBatch"].join(dot);
	public static readonly lightmapIndex = [meshRenderer, "lightmapIndex"].join(dot);
	public static readonly realtimeLightmapIndex = [meshRenderer, "realtimeLightmapIndex"].join(
		dot,
	);

	public static readonly tag = [meshRenderer, tag].join(dot);
	public static readonly Name = [meshRenderer, nameField].join(dot);
}
