import {
	dot,
	enabled,
	isActiveAndEnabled,
	nameField,
	runInEditMode,
	tag,
	useGUILayout,
} from "./commonFields";

const button = "Button";

export class ButtonField {
	public static readonly value = button;
	public static readonly interactable = [button, "interactable"].join(dot);
	public static readonly useGUILayout = [button, useGUILayout].join(dot);
	public static readonly runInEditMode = [button, runInEditMode].join(dot);

	public static readonly enabled = [button, enabled].join(dot);
	public static readonly isActiveAndEnabled = [button, isActiveAndEnabled].join(dot);
	public static readonly tag = [button, tag].join(dot);
	public static readonly Name = [button, nameField].join(dot);
}
