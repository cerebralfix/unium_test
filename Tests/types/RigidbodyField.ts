import { dot, nameField, tag } from "./commonFields";

const rigidBody = "Rigidbody";

export class RigidbodyField {
	public static readonly value = rigidBody;

	public static readonly drag = [rigidBody, "drag"].join(dot);
	public static readonly angularDrag = [rigidBody, "angularDrag"].join(dot);
	public static readonly mass = [rigidBody, "mass"].join(dot);
	public static readonly useGravity = [rigidBody, "useGravity"].join(dot);
	public static readonly maxDepenetrationVelocity = [rigidBody, "maxDepenetrationVelocity"].join(
		dot,
	);
	public static readonly isKinematic = [rigidBody, "isKinematic"].join(dot);
	public static readonly freezeRotation = [rigidBody, "freezeRotation"].join(dot);
	public static readonly detectCollisions = [rigidBody, "detectCollisions"].join(dot);
	public static readonly solverIterations = [rigidBody, "solverIterations"].join(dot);
	public static readonly sleepThreshold = [rigidBody, "sleepThreshold"].join(dot);
	public static readonly maxAngularVelocity = [rigidBody, "maxAngularVelocity"].join(dot);
	public static readonly solverVelocityIterations = [rigidBody, "solverVelocityIterations"].join(
		dot,
	);

	public static readonly tag = [rigidBody, tag].join(dot);
	public static readonly Name = [rigidBody, nameField].join(dot);
}
