import { dot, isPlaying, nameField, tag, time } from "./commonFields";

const particleSystem = "ParticleSystem";

export class ParticleSystemField {
	public static readonly value = particleSystem;

	public static readonly isPlaying = [particleSystem, isPlaying].join(dot);
	public static readonly isEmitting = [particleSystem, "isEmitting"].join(dot);
	public static readonly isStopped = [particleSystem, "isStopped"].join(dot);
	public static readonly isPaused = [particleSystem, "isPaused"].join(dot);
	public static readonly time = [particleSystem, time].join(dot);
	public static readonly particleCount = [particleSystem, "particleCount"].join(dot);
	public static readonly randomSeed = [particleSystem, "randomSeed"].join(dot);
	public static readonly useAutoRandomSeed = [particleSystem, "useAutoRandomSeed"].join(dot);
	public static readonly automaticCullingEnabled = [
		particleSystem,
		"automaticCullingEnabled",
	].join(dot);

	public static readonly tag = [particleSystem, tag].join(dot);
	public static readonly Name = [particleSystem, nameField].join(dot);
}
