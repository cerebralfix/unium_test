export const dot = ".";

// Image
export const pixelsPerUnit = "pixelsPerUnit";
export const minWidth = "minWidth";
export const preferredWidth = "preferredWidth";
export const flexibleWidth = "flexibleWidth";
export const minHeight = "minHeight";
export const preferredHeight = "preferredHeight";
export const flexibleHeight = "flexibleHeight";
export const layoutPriority = "layoutPriority";
export const maskable = "maskable";
export const raycastTarget = "raycastTarget";
export const depth = "depth";
export const useGUILayout = "useGUILayout";
export const runInEditMode = "runInEditMode";
export const enabled = "enabled";
export const isActiveAndEnabled = "isActiveAndEnabled";
export const tag = "tag";
export const nameField = "name";

// Transform
export const position = "position";
export const rotation = "rotation";
export const scale = "scale";
export const x = "x";
export const y = "y";
export const z = "z";
export const w = "w";

// Other
export const cullingMask = "cullingMask";
export const volume = "volume";
export const time = "time";
export const isPlaying = "isPlaying";
export const isVisible = "isVisible";
export const isTrigger = "isTrigger";
