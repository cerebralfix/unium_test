import { dot, enabled, isTrigger, nameField, tag } from "./commonFields";

const sphereCollider = "SphereCollider";

export class SphereColliderField {
	public static readonly value = sphereCollider;
	public static readonly radius = [sphereCollider, "radius"].join(dot);
	public static readonly enabled = [sphereCollider, enabled].join(dot);
	public static readonly isTrigger = [sphereCollider, isTrigger].join(dot);
	public static readonly contactOffset = [sphereCollider, "contactOffset"].join(dot);

	public static readonly tag = [sphereCollider, tag].join(dot);
	public static readonly Name = [sphereCollider, nameField].join(dot);
}
