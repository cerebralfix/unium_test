import { dot, nameField, position, rotation, scale, w, x, y, z } from "./commonFields";

const rectTransform = "RectTransform";

export class RectTransformField {
	public static readonly value = rectTransform;
	public static readonly Name = [rectTransform, nameField].join(dot);
	public static readonly position = {
		value: [rectTransform, position].join(dot),
		x: [rectTransform, position, x].join(dot),
		y: [rectTransform, position, y].join(dot),
		z: [rectTransform, position, z].join(dot),
	};
	public static readonly rotation = {
		value: [rectTransform, rotation].join(dot),
		x: [rectTransform, rotation, x].join(dot),
		y: [rectTransform, rotation, y].join(dot),
		z: [rectTransform, rotation, z].join(dot),
		w: [rectTransform, rotation, w].join(dot),
	};
	public static readonly scale = {
		value: [rectTransform, scale].join(dot),
		x: [rectTransform, scale, x].join(dot),
		y: [rectTransform, scale, y].join(dot),
		z: [rectTransform, scale, z].join(dot),
	};
}
