import { dot, nameField, position, rotation, scale, w, x, y, z } from "./commonFields";

const transform = "Transform";

export class TransformField {
	public static readonly value = transform;
	public static readonly Name = [transform, nameField].join(dot);
	public static readonly position = {
		value: [transform, position].join(dot),
		x: [transform, position, x].join(dot),
		y: [transform, position, y].join(dot),
		z: [transform, position, z].join(dot),
	};
	public static readonly rotation = {
		value: [transform, rotation].join(dot),
		x: [transform, rotation, x].join(dot),
		y: [transform, rotation, y].join(dot),
		z: [transform, rotation, z].join(dot),
		w: [transform, rotation, w].join(dot),
	};
	public static readonly scale = {
		value: [transform, scale].join(dot),
		x: [transform, scale, x].join(dot),
		y: [transform, scale, y].join(dot),
		z: [transform, scale, z].join(dot),
	};
}
