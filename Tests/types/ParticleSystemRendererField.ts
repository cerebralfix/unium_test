import { dot, enabled, isVisible, nameField, tag } from "./commonFields";

const particleSystemRenderer = "ParticleSystemRenderer";

export class ParticleSystemRendererField {
	public static readonly value = particleSystemRenderer;
	public static readonly meshCount = [particleSystemRenderer, "meshCount"].join(dot);
	public static readonly activeVertexStreamsCount = [
		particleSystemRenderer,
		"activeVertexStreamsCount",
	].join(dot);
	public static readonly lengthScale = [particleSystemRenderer, "lengthScale"].join(dot);
	public static readonly velocityScale = [particleSystemRenderer, "velocityScale"].join(dot);
	public static readonly cameraVelocityScale = [
		particleSystemRenderer,
		"cameraVelocityScale",
	].join(dot);
	public static readonly normalDirection = [particleSystemRenderer, "normalDirection"].join(dot);
	public static readonly sortingFudge = [particleSystemRenderer, "sortingFudge"].join(dot);
	public static readonly minParticleSize = [particleSystemRenderer, "minParticleSize"].join(dot);
	public static readonly maxParticleSize = [particleSystemRenderer, "maxParticleSize"].join(dot);
	public static readonly enableGPUInstancing = [
		particleSystemRenderer,
		"enableGPUInstancing",
	].join(dot);
	public static readonly enabled = [particleSystemRenderer, enabled].join(dot);
	public static readonly isVisible = [particleSystemRenderer, isVisible].join(dot);
	public static readonly receiveShadows = [particleSystemRenderer, "receiveShadows"].join(dot);
	public static readonly renderingLayerMask = [particleSystemRenderer, "renderingLayerMask"].join(
		dot,
	);
	public static readonly sortingLayerName = [particleSystemRenderer, "sortingLayerName"].join(
		dot,
	);
	public static readonly sortingLayerID = [particleSystemRenderer, "sortingLayerID"].join(dot);
	public static readonly sortingOrder = [particleSystemRenderer, "sortingOrder"].join(dot);
	public static readonly allowOcclusionWhenDynamic = [
		particleSystemRenderer,
		"allowOcclusionWhenDynamic",
	].join(dot);
	public static readonly isPartOfStaticBatch = [
		particleSystemRenderer,
		"isPartOfStaticBatch",
	].join(dot);
	public static readonly lightmapIndex = [particleSystemRenderer, "lightmapIndex"].join(dot);
	public static readonly realtimeLightmapIndex = [
		particleSystemRenderer,
		"realtimeLightmapIndex",
	].join(dot);

	public static readonly tag = [particleSystemRenderer, tag].join(dot);
	public static readonly Name = [particleSystemRenderer, nameField].join(dot);
}
