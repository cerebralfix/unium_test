import {
	dot,
	enabled,
	isActiveAndEnabled,
	isPlaying,
	nameField,
	tag,
	time,
	volume,
} from "./commonFields";

const audioSource = "AudioSource";

export class AudioSourceField {
	public static readonly value = audioSource;
	public static readonly volume = [audioSource, volume].join(dot);
	public static readonly pitch = [audioSource, "pitch"].join(dot);
	public static readonly time = [audioSource, time].join(dot);
	public static readonly timeSamples = [audioSource, "timeSamples"].join(dot);
	public static readonly isPlaying = [audioSource, isPlaying].join(dot);
	public static readonly isVirtual = [audioSource, "isVirtual"].join(dot);
	public static readonly loop = [audioSource, "loop"].join(dot);
	public static readonly ignoreListenerVolume = [audioSource, "ignoreListenerVolume"].join(dot);
	public static readonly playOnAwake = [audioSource, "playOnAwake"].join(dot);
	public static readonly ignoreListenerPause = [audioSource, "ignoreListenerPause"].join(dot);
	public static readonly panStereo = [audioSource, "panStereo"].join(dot);
	public static readonly spatialBlend = [audioSource, "spatialBlend"].join(dot);
	public static readonly spatialize = [audioSource, "spatialize"].join(dot);
	public static readonly spatializePostEffects = [audioSource, "spatializePostEffects"].join(dot);
	public static readonly reverbZoneMix = [audioSource, "reverbZoneMix"].join(dot);
	public static readonly bypassEffects = [audioSource, "bypassEffects"].join(dot);
	public static readonly bypassListenerEffects = [audioSource, "bypassListenerEffects"].join(dot);
	public static readonly bypassReverbZones = [audioSource, "bypassReverbZones"].join(dot);
	public static readonly dopplerLevel = [audioSource, "dopplerLevel"].join(dot);
	public static readonly spread = [audioSource, "spread"].join(dot);
	public static readonly priority = [audioSource, "priority"].join(dot);
	public static readonly mute = [audioSource, "mute"].join(dot);
	public static readonly minDistance = [audioSource, "minDistance"].join(dot);
	public static readonly maxDistance = [audioSource, "maxDistance"].join(dot);

	public static readonly enabled = [audioSource, enabled].join(dot);
	public static readonly isActiveAndEnabled = [audioSource, isActiveAndEnabled].join(dot);
	public static readonly tag = [audioSource, tag].join(dot);
	public static readonly Name = [audioSource, nameField].join(dot);
}
