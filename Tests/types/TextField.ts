import {
	depth,
	dot,
	enabled,
	flexibleHeight,
	flexibleWidth,
	isActiveAndEnabled,
	layoutPriority,
	maskable,
	minHeight,
	minWidth,
	nameField,
	pixelsPerUnit,
	preferredHeight,
	preferredWidth,
	raycastTarget,
	runInEditMode,
	tag,
	useGUILayout,
} from "./commonFields";

const textField = "Text";

export class TextField {
	public static readonly value = textField;
	public static readonly text = [textField, "text"].join(dot);
	public static readonly supportRichText = [textField, "supportRichText"].join(dot);
	public static readonly resizeTextForBestFit = [textField, "resizeTextForBestFit"].join(dot);
	public static readonly resizeTextMinSize = [textField, "resizeTextMinSize"].join(dot);
	public static readonly resizeTextMaxSize = [textField, "resizeTextMaxSize"].join(dot);
	public static readonly alignByGeometry = [textField, "alignByGeometry"].join(dot);
	public static readonly fontSize = [textField, "fontSize"].join(dot);
	public static readonly lineSpacing = [textField, "lineSpacing"].join(dot);
	public static readonly pixelsPerUnit = [textField, pixelsPerUnit].join(dot);
	public static readonly minWidth = [textField, minWidth].join(dot);
	public static readonly preferredWidth = [textField, preferredWidth].join(dot);
	public static readonly flexibleWidth = [textField, flexibleWidth].join(dot);
	public static readonly minHeight = [textField, minHeight].join(dot);
	public static readonly preferredHeight = [textField, preferredHeight].join(dot);
	public static readonly flexibleHeight = [textField, flexibleHeight].join(dot);
	public static readonly layoutPriority = [textField, layoutPriority].join(dot);
	public static readonly maskable = [textField, maskable].join(dot);
	public static readonly raycastTarget = [textField, raycastTarget].join(dot);
	public static readonly depth = [textField, depth].join(dot);
	public static readonly useGUILayout = [textField, useGUILayout].join(dot);
	public static readonly runInEditMode = [textField, runInEditMode].join(dot);
	public static readonly enabled = [textField, enabled].join(dot);
	public static readonly isActiveAndEnabled = [textField, isActiveAndEnabled].join(dot);
	public static readonly tag = [textField, tag].join(dot);
	public static readonly Name = [textField, nameField].join(dot);
}
