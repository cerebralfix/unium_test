import {
	cullingMask,
	depth,
	dot,
	enabled,
	isActiveAndEnabled,
	nameField,
	tag,
} from "./commonFields";

const camera = "Camera";

export class CameraField {
	public static readonly value = camera;
	public static readonly nearClipPlane = [camera, "nearClipPlane"].join(dot);
	public static readonly farClipPlane = [camera, "farClipPlane"].join(dot);
	public static readonly fieldOfView = [camera, "fieldOfView"].join(dot);
	public static readonly allowHDR = [camera, "allowHDR"].join(dot);
	public static readonly allowMSAA = [camera, "allowMSAA"].join(dot);
	public static readonly allowDynamicResolution = [camera, "allowDynamicResolution"].join(dot);
	public static readonly forceIntoRenderTexture = [camera, "forceIntoRenderTexture"].join(dot);
	public static readonly orthographicSize = [camera, "orthographicSize"].join(dot);
	public static readonly orthographic = [camera, "orthographic"].join(dot);
	public static readonly depth = [camera, depth].join(dot);
	public static readonly aspect = [camera, "aspect"].join(dot);
	public static readonly cullingMask = [camera, cullingMask].join(dot);
	public static readonly eventMask = [camera, "eventMask"].join(dot);
	public static readonly layerCullSpherical = [camera, "layerCullSpherical"].join(dot);
	public static readonly useOcclusionCulling = [camera, "useOcclusionCulling"].join(dot);
	public static readonly clearStencilAfterLightingPass = [
		camera,
		"clearStencilAfterLightingPass",
	].join(dot);
	public static readonly usePhysicalProperties = [camera, "usePhysicalProperties"].join(dot);
	public static readonly focalLength = [camera, "focalLength"].join(dot);
	public static readonly pixelWidth = [camera, "pixelWidth"].join(dot);
	public static readonly pixelHeight = [camera, "pixelHeight"].join(dot);
	public static readonly scaledPixelWidth = [camera, "scaledPixelWidth"].join(dot);
	public static readonly scaledPixelHeight = [camera, "scaledPixelHeight"].join(dot);
	public static readonly targetDisplay = [camera, "targetDisplay"].join(dot);
	public static readonly useJitteredProjectionMatrixForTransparentRendering = [
		camera,
		"useJitteredProjectionMatrixForTransparentRendering",
	].join(dot);
	public static readonly stereoEnabled = [camera, "stereoEnabled"].join(dot);
	public static readonly stereoSeparation = [camera, "stereoSeparation"].join(dot);
	public static readonly stereoConvergence = [camera, "stereoConvergence"].join(dot);
	public static readonly areVRStereoViewMatricesWithinSingleCullTolerance = [
		camera,
		"areVRStereoViewMatricesWithinSingleCullTolerance",
	].join(dot);
	public static readonly allCamerasCount = [camera, "allCamerasCount"].join(dot);
	public static readonly commandBufferCount = [camera, "commandBufferCount"].join(dot);

	public static readonly enabled = [camera, enabled].join(dot);
	public static readonly isActiveAndEnabled = [camera, isActiveAndEnabled].join(dot);
	public static readonly tag = [camera, tag].join(dot);
	public static readonly Name = [camera, nameField].join(dot);
}
