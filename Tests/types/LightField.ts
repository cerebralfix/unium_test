import { cullingMask, dot, enabled, isActiveAndEnabled, nameField, tag } from "./commonFields";

const light = "Light";

export class LightField {
	public static readonly value = light;
	public static readonly shadowStrength = [light, "shadowStrength"].join(dot);
	public static readonly cookieSize = [light, "cookieSize"].join(dot);
	public static readonly commandBufferCount = [light, "commandBufferCount"].join(dot);
	public static readonly spotAngle = [light, "spotAngle"].join(dot);
	public static readonly colorTemperature = [light, "colorTemperature"].join(dot);
	public static readonly intensity = [light, "intensity"].join(dot);
	public static readonly bounceIntensity = [light, "bounceIntensity"].join(dot);
	public static readonly shadowCustomResolution = [light, "shadowCustomResolution"].join(dot);
	public static readonly shadowBias = [light, "shadowBias"].join(dot);
	public static readonly shadowNormalBias = [light, "shadowNormalBias"].join(dot);
	public static readonly shadowNearPlane = [light, "shadowNearPlane"].join(dot);
	public static readonly range = [light, "range"].join(dot);
	public static readonly cullingMask = [light, cullingMask].join(dot);
	public static readonly shadowRadius = [light, "shadowRadius"].join(dot);
	public static readonly shadowAngle = [light, "shadowAngle"].join(dot);
	public static readonly enabled = [light, enabled].join(dot);
	public static readonly isActiveAndEnabled = [light, isActiveAndEnabled].join(dot);
	public static readonly tag = [light, tag].join(dot);
	public static readonly Name = [light, nameField].join(dot);
}
