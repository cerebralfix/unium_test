import { dot, enabled, isTrigger, nameField, tag } from "./commonFields";

const meshCollider = "MeshCollider";

export class MeshColliderField {
	public static readonly value = meshCollider;

	public static readonly convex = [meshCollider, "convex"].join(dot);
	public static readonly inflateMesh = [meshCollider, "inflateMesh"].join(dot);
	public static readonly skinWidth = [meshCollider, "skinWidth"].join(dot);
	public static readonly enabled = [meshCollider, enabled].join(dot);
	public static readonly isTrigger = [meshCollider, isTrigger].join(dot);
	public static readonly contactOffset = [meshCollider, "contactOffset"].join(dot);

	public static readonly tag = [meshCollider, tag].join(dot);
	public static readonly Name = [meshCollider, nameField].join(dot);
}
