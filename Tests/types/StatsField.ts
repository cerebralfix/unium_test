import { dot } from "./commonFields";

const statsField = "stats";

export class StatsField {
	public static readonly value = statsField;
	public static readonly FPS = [statsField, "FPS"].join(dot);
	public static readonly RunningTime = [statsField, "RunningTime"].join(dot);
	public static readonly LevelTime = [statsField, "LevelTime"].join(dot);
	public static readonly Scene = [statsField, "Scene"].join(dot);
}
