import { dot, nameField, tag } from "./commonFields";

const canvasRenderer = "CanvasRenderer";

export class CanvasRendererField {
	public static readonly value = canvasRenderer;
	public static readonly hasPopInstruction = [canvasRenderer, "hasPopInstruction"].join(dot);
	public static readonly materialCount = [canvasRenderer, "materialCount"].join(dot);
	public static readonly popMaterialCount = [canvasRenderer, "popMaterialCount"].join(dot);
	public static readonly absoluteDepth = [canvasRenderer, "absoluteDepth"].join(dot);
	public static readonly hasMoved = [canvasRenderer, "hasMoved"].join(dot);
	public static readonly cullTransparentMesh = [canvasRenderer, "cullTransparentMesh"].join(dot);
	public static readonly hasRectClipping = [canvasRenderer, "hasRectClipping"].join(dot);
	public static readonly relativeDepth = [canvasRenderer, "relativeDepth"].join(dot);
	public static readonly cull = [canvasRenderer, "cull"].join(dot);
	public static readonly tag = [canvasRenderer, tag].join(dot);
	public static readonly Name = [canvasRenderer, nameField].join(dot);
}
