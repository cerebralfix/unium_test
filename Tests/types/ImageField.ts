import {
	depth,
	dot,
	enabled,
	flexibleHeight,
	flexibleWidth,
	isActiveAndEnabled,
	layoutPriority,
	maskable,
	minHeight,
	minWidth,
	nameField,
	pixelsPerUnit,
	preferredHeight,
	preferredWidth,
	raycastTarget,
	runInEditMode,
	tag,
	useGUILayout,
} from "./commonFields";

const image = "Image";

export class ImageField {
	public static readonly value = image;
	public static readonly preserveAspect = [image, "preserveAspect"].join(dot);
	public static readonly fillCenter = [image, "fillCenter"].join(dot);
	public static readonly fillAmount = [image, "fillAmount"].join(dot);
	public static readonly fillClockwise = [image, "fillClockwise"].join(dot);
	public static readonly fillOrigin = [image, "fillOrigin"].join(dot);
	public static readonly alphaHitTestMinimumThreshold = [
		image,
		"alphaHitTestMinimumThreshold",
	].join(dot);
	public static readonly hasBorder = [image, "hasBorder"].join(dot);
	public static readonly pixelsPerUnit = [image, pixelsPerUnit].join(dot);
	public static readonly minWidth = [image, minWidth].join(dot);
	public static readonly preferredWidth = [image, preferredWidth].join(dot);
	public static readonly flexibleWidth = [image, flexibleWidth].join(dot);
	public static readonly minHeight = [image, minHeight].join(dot);
	public static readonly preferredHeight = [image, preferredHeight].join(dot);
	public static readonly flexibleHeight = [image, flexibleHeight].join(dot);
	public static readonly layoutPriority = [image, layoutPriority].join(dot);
	public static readonly maskable = [image, maskable].join(dot);
	public static readonly raycastTarget = [image, raycastTarget].join(dot);
	public static readonly depth = [image, depth].join(dot);
	public static readonly useGUILayout = [image, useGUILayout].join(dot);
	public static readonly runInEditMode = [image, runInEditMode].join(dot);
	public static readonly enabled = [image, enabled].join(dot);
	public static readonly isActiveAndEnabled = [image, isActiveAndEnabled].join(dot);
	public static readonly tag = [image, tag].join(dot);
	public static readonly Name = [image, nameField].join(dot);
}
