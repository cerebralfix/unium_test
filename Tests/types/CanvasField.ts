import { dot, enabled, isActiveAndEnabled, nameField, tag } from "./commonFields";

const canvas = "Canvas";

export class CanvasField {
	public static readonly value = canvas;
	public static readonly isRootCanvas = [canvas, "isRootCanvas"].join(dot);
	public static readonly scaleFactor = [canvas, "scaleFactor"].join(dot);
	public static readonly referencePixelsPerUnit = [canvas, "referencePixelsPerUnit"].join(dot);
	public static readonly overridePixelPerfect = [canvas, "overridePixelPerfect"].join(dot);
	public static readonly pixelPerfect = [canvas, "pixelPerfect"].join(dot);
	public static readonly planeDistance = [canvas, "planeDistance"].join(dot);
	public static readonly renderOrder = [canvas, "renderOrder"].join(dot);
	public static readonly overrideSorting = [canvas, "overrideSorting"].join(dot);
	public static readonly sortingOrder = [canvas, "sortingOrder"].join(dot);
	public static readonly targetDisplay = [canvas, "targetDisplay"].join(dot);
	public static readonly sortingLayerID = [canvas, "sortingLayerID"].join(dot);
	public static readonly cachedSortingLayerValue = [canvas, "cachedSortingLayerValue"].join(dot);
	public static readonly sortingLayerName = [canvas, "sortingLayerName"].join(dot);
	public static readonly normalizedSortingGridSize = [canvas, "normalizedSortingGridSize"].join(
		dot,
	);

	public static readonly enabled = [canvas, enabled].join(dot);
	public static readonly isActiveAndEnabled = [canvas, isActiveAndEnabled].join(dot);
	public static readonly tag = [canvas, tag].join(dot);
	public static readonly Name = [canvas, nameField].join(dot);
}
