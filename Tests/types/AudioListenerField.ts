import { dot, enabled, isActiveAndEnabled, nameField, tag, volume } from "./commonFields";

const audioListener = "AudioListener";

export class AudioListenerField {
	public static readonly value = audioListener;
	public static readonly volume = [audioListener, volume].join(dot);
	public static readonly pause = [audioListener, "pause"].join(dot);
	public static readonly enabled = [audioListener, enabled].join(dot);
	public static readonly isActiveAndEnabled = [audioListener, isActiveAndEnabled].join(dot);
	public static readonly tag = [audioListener, tag].join(dot);
	public static readonly Name = [audioListener, nameField].join(dot);
}
