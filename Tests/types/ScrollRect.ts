import {
	dot,
	enabled,
	flexibleHeight,
	flexibleWidth,
	isActiveAndEnabled,
	layoutPriority,
	minHeight,
	minWidth,
	nameField,
	preferredHeight,
	preferredWidth,
	runInEditMode,
	tag,
	useGUILayout,
} from "./commonFields";

const scrollRect = "ScrollRect";

export class ScrollRect {
	public static readonly value = scrollRect;
	public static readonly horizontal = [scrollRect, "horizontal"].join(dot);
	public static readonly vertical = [scrollRect, "vertical"].join(dot);
	public static readonly elasticity = [scrollRect, "elasticity"].join(dot);
	public static readonly inertia = [scrollRect, "inertia"].join(dot);
	public static readonly decelerationRate = [scrollRect, "decelerationRate"].join(dot);
	public static readonly scrollSensitivity = [scrollRect, "scrollSensitivity"].join(dot);
	public static readonly horizontalScrollbarSpacing = [scrollRect, "horizontalScrollbarSpacing"].join(dot);
	public static readonly verticalScrollbarSpacing = [scrollRect, "verticalScrollbarSpacing"].join(dot);
	public static readonly horizontalNormalizedPosition = [scrollRect, "horizontalNormalizedPosition"].join(dot);
	public static readonly verticalNormalizedPosition = [scrollRect, "verticalNormalizedPosition"].join(dot);
	public static readonly minWidth = [scrollRect, minWidth].join(dot);
	public static readonly preferredWidth = [scrollRect, preferredWidth].join(dot);
	public static readonly flexibleWidth = [scrollRect, flexibleWidth].join(dot);
	public static readonly minHeight = [scrollRect, minHeight].join(dot);
	public static readonly preferredHeight = [scrollRect, preferredHeight].join(dot);
	public static readonly flexibleHeight = [scrollRect, flexibleHeight].join(dot);
	public static readonly layoutPriority = [scrollRect, layoutPriority].join(dot);
	public static readonly useGUILayout = [scrollRect, useGUILayout].join(dot);
	public static readonly runInEditMode = [scrollRect, runInEditMode].join(dot);
	public static readonly enabled = [scrollRect, enabled].join(dot);
	public static readonly isActiveAndEnabled = [scrollRect, isActiveAndEnabled].join(dot);
	public static readonly tag = [scrollRect, tag].join(dot);
	public static readonly Name = [scrollRect, nameField].join(dot);
}
