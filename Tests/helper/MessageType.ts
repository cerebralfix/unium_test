export interface IQueryMessage {
	id: string;
	q: string;
	repeat?: {
		freq?: number;
		skip?: number;
		samples?: number;
	};
}

export interface IReplyMessage {
	id: string;
	data: any;
	info?: any;
	error?: any;
}

export interface IUniumObject {
	Unium: string;
	Unity?: string;
	Mono?: string;
	IsEditor: boolean;
	Product: string;
	Company: string;
	Version: string;
	IPAddress?: string;
	FPS?: number;
	RunningTime?: number;
	Scene: string;
}

export interface IPosition {
	x: number;
	y: number;
	z: number;
}
