import { EventEmitter } from "events";
import { TransformField } from "../types/TransformField";
import { Logger } from "./Logger";
import { IPosition, IQueryMessage, IReplyMessage, IUniumObject } from "./MessageType";

export default class WebSocketHelper {
	public isEditor: boolean;

	private ws: WebSocket = null;
	private url: string;
	private eventEmitter: EventEmitter = new EventEmitter();
	private repeatingQueries = {};

	private readonly prefixQuery: string = "/q/";

	constructor() {
		this.url =
			"ws://" +
			(process.env.IP === undefined ? "localhost" : process.env.IP) +
			":" +
			(process.env.PORT === undefined ? "8342" : process.env.PORT) +
			"/ws";
		Logger.log("URL: " + this.url);
		this.setIsEditor();
	}

	public async about(): Promise<IUniumObject> {
		return this.send("about", "/about");
	}

	public async getGameObjectPosition(object: string): Promise<IPosition> {
		return await this.singleQuery<IPosition>(
			`${object}_position_${Math.random()}`,
			`${object}.${TransformField.position.value}`,
		);
	}

	public async teleportObjectToPosition(object: string, position: IPosition): Promise<undefined> {
		const teleportTo: string =
			`${TransformField.position.value}={'x':` +
			position.x +
			", 'y':" +
			position.y +
			", 'z':" +
			position.z +
			"}";
		return this.singleQuery<undefined>(
			`teleport_${object}_${Math.random()}`,
			`${object}.${teleportTo}`,
		);
	}

	/**
	 * Send a query and will wait for any response and return first result.
	 */
	public async query<T>(url: string): Promise<T> {
		return this.singleQuery<T>(`query_${url}_${Math.random()}`, url);
	}
	/**
	 * Send a query and will wait for any response and return all results.
	 */
	public async queryAll(id: string, url: string): Promise<any> {
		return this.send(id, this.prefixQuery + url);
	}

	/**
	 * Send a query that is repeating and validates the response.
	 *
	 * freq is is seconds.
	 * To sample at 4 times a second freq = 0.25.
	 * To sample every frame freq = 0.
	 */
	public async repeatQuery(id: string, url: string, freq: number = 1): Promise<any> {
		this.addRepeatingQueryId(id);
		return this.send(id, this.prefixQuery + url, freq, "repeating");
	}

	/**
	 * When a repeating query is no longer required this will stop it
	 * from returning any more responses.
	 */
	public async removeQuery(id: string): Promise<void> {
		await this.send(id, "/socket.stop(" + id + ")", -1, "stopped");
		delete this.repeatingQueries[id];
	}

	/**
	 * Sleep a certain milliseconds
	 */
	public async sleep(ms: number): Promise<{}> {
		return new Promise((resolve) => setTimeout(resolve, ms));
	}

	/**
	 * Invoke onClick function on Button
	 */
	public async invokeClick(object: string): Promise<undefined> {
		return this.singleQuery<undefined>(
			`invoke_click_${object}_${Math.random()}`,
			`${object}.Button.onClick.Invoke()`,
		);
	}

	/**
	 * Simulates a click.
	 *
	 * Will simulates a click at x, y position.
	 */
	public async simulateClick(x: number, y: number): Promise<void> {
		if (x < 0 || y < 0) {
			throw { error: "invalid input range" };
		} else {
			const newX: number = Math.round(x);
			const newY: number = Math.round(y);
			await this.singleQuery(
				`click_${newX}_${newY}_${Math.random()}`,
				`simulate.click(${newX}, ${newY})`,
			);
		}
	}

	/**
	 * Simulates a click in normalised screen.
	 *
	 * Will simulates a click at x, y position.
	 * x and y value must be between 0 and 1
	 *
	 */
	public async simulateClickNormalised(x: number, y: number): Promise<void> {
		if (x < 0 || x > 1 || y < 0 || y > 1) {
			throw { error: "invalid input range" };
		} else {
			await this.singleQuery(
				`click_${x}_${y}_${Math.random()}`,
				`simulate.clickNormalised(${x}, ${y})`,
			);
		}
	}

	/**
	 * Simulates a drag.
	 *
	 * Will simulates a drag from fromX, fromY to toX, toY position in durationS seconds
	 */
	public async simulateDrag(
		fromX: number,
		fromY: number,
		toX: number,
		toY: number,
		durationS: number,
	): Promise<void> {
		if (fromX < 0 || fromY < 0 || toX < 0 || toY < 0 || durationS < 0) {
			throw { error: "invalid input range" };
		} else {
			const newFromX: number = Math.round(fromX);
			const newFromY: number = Math.round(fromY);
			const newToX: number = Math.round(toX);
			const newToY: number = Math.round(toY);
			await this.singleQuery(
				`drag_${newFromX}_${newFromY}_${newToX}_${newToX}_${durationS}_${Math.random()}`,
				`simulate.drag(${newFromX}, ${newFromY}, ${newToX}, ${newToY}, ${durationS})`,
			);
		}
	}

	/**
	 * Simulates a drag in normalised screen.
	 *
	 * Will simulates a drag from fromX, fromY to toX, toY position in durationS seconds
	 * x and y value must be between 0 and 1
	 *
	 */
	public async simulateDragNormalised(
		fromX: number,
		fromY: number,
		toX: number,
		toY: number,
		durationS: number,
	): Promise<void> {
		if (
			fromX < 0 ||
			fromY < 0 ||
			fromX > 1 ||
			fromY > 1 ||
			toX < 0 ||
			toY < 0 ||
			toX > 1 ||
			toY > 1 ||
			durationS < 0
		) {
			throw { error: "invalid input range" };
		} else {
			await this.singleQuery(
				`drag_${fromX}_${fromY}_${toX}_${toY}_${durationS}_${Math.random()}`,
				`simulate.dragNormalised(${fromX}, ${fromY}, ${toX}, ${toY}, ${durationS})`,
			);
		}
	}

	public connect(): Promise<void> {
		return new Promise((result, reject) => {
			this.ws = new WebSocket(this.url);
			this.ws.onopen = () => result();
			this.ws.onerror = () => reject("Failed to Connect");
			this.ws.onmessage = (m) => {
				const msg: IReplyMessage = JSON.parse(m.data);
				Logger.log("Received message: " + JSON.stringify(msg));

				if (msg.error) {
					Logger.log("Message has an error: {" + msg.id + "} " + msg.error);
				}

				// Unium returns 'data' in an array which has only one entry.
				// We return that entry to save having to access it on the listeners.
				// Events return an 'object'.
				// We check for the prefixEvent in order to determine how we should access/return the data
				this.eventEmitter.emit(msg.id, msg.info || msg.data);
			};
		});
	}

	public close(): void {
		this.ws.close();
	}

	public async waitForResponse(
		id: string,
		expectedResponse: string = null,
		repeating: boolean = false,
	): Promise<any> {
		return new Promise((resolve, reject) => {
			const listener = (response) => {
				Logger.log(
					"waitForResponseListener: response=" +
						response +
						" expectedResponse=" +
						expectedResponse +
						" repeating=" +
						repeating,
				);
				if (!expectedResponse || expectedResponse === response) {
					this.eventEmitter.removeListener(id, listener);
					resolve(response);
				} else if (!repeating) {
					this.eventEmitter.removeListener(id, listener);
					reject(
						"Returned response (" +
							response +
							") does not match expected (" +
							expectedResponse +
							")",
					);
				}
			};
			this.eventEmitter.on(id, listener);
		});
	}

	private async send(
		id: string,
		url: string,
		freq: number = -1,
		expectedResponse: string = null,
	): Promise<any> {
		const msg: IQueryMessage = { id, q: url };
		if (freq >= 0) {
			msg.repeat = { freq };
		}
		Logger.log("Sending message: " + JSON.stringify(msg));

		Logger.log("Ready state ", this.ws.readyState);

		this.ws.send(JSON.stringify(msg));
		return this.waitForResponse(id, expectedResponse);
	}

	/**
	 * Validates that a repeating query with the specified id is not already in use.
	 * Throws an exception if the id is already in use.
	 */
	private addRepeatingQueryId(id: string): void {
		if (this.repeatingQueries[id] === 1) {
			throw {
				error: `Repeating query ID (${id}) is already being used`,
			};
		}

		this.repeatingQueries[id] = 1;
	}

	private async singleQuery<T>(id: string, url: string): Promise<T> {
		const queryAllResult = await this.queryAll(id, url);
		return queryAllResult[0] as T;
	}

	private async setIsEditor(): Promise<void> {
		this.isEditor = (await this.about()).IsEditor;
	}
}
