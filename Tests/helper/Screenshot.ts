import * as fs from "fs";
import * as path from "path";
import * as request from "request";

export function screenshot(outputLocation): Promise<void> {
	return new Promise((resolve, reject) => {
		const dir: string = path.dirname(outputLocation);
		if (!fs.existsSync(dir)) {
			fs.mkdirSync(dir);
		}
		request("http://localhost:8342/utils/appscreenshot")
			.pipe(fs.createWriteStream(outputLocation))
			.on("error", (err) => {
				reject("Failed to take screenshot: " + err);
			})
			.on("finish", () => resolve());
	});
}
