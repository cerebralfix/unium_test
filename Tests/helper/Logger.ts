export class Logger {
	public static log(...output): void {
		if (this.logOutput) {
			console.log(output);
		}
	}

	private static logOutput = process.env.DEBUG === "true";
}
