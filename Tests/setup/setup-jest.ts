import WebSocketHelper from "../helper/WSHelper";

export const wsh = new WebSocketHelper();

beforeAll(async () => {
	await wsh.connect();
});

afterAll(() => {
	wsh.close();
});
