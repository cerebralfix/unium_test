using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public static class HierarchyUtil
{
	[MenuItem("GameObject/Copy Hierarchy to Clipboard", false, 0)]
	private static void CopyHierarchyToClipboard()
	{
		Transform t = Selection.activeGameObject.transform;
		string result = t.name;
		while (t.parent != null)
		{
			t = t.parent;
			result = t.name + "/" + result;
		}
		
		EditorGUIUtility.systemCopyBuffer = result;
	}
}
