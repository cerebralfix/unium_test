﻿using UnityEditor;

class AutomationEditor
{

    // Use this for initialization
    public static void PlayInEditor()
    {
        System.Console.WriteLine("Running PlayInEditor");

        #if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = true;
        #endif
    }

}
