module.exports = {
	transform: {
		".ts": "<rootDir>/node_modules/ts-jest",
	},
	globals: {
		"ts-jest": {
			tsConfig: "tsconfig.json",
		},
	},
	moduleFileExtensions: ["js", "ts"],
	testRegex: "./Tests/innerTest.*\\.spec\\.ts$",
	collectCoverage: true,
	collectCoverageFrom: ["**/helper/WSHelper.ts", "**/types/*.ts"],
	coverageReporters: ["json-summary", "lcov"],
};
